set -e

demo/buildtree.sh
rm -r build

demo/installtree.sh
rm -r build

demo/conan.sh

echo "Success"
