set -e

ROOT=$(pwd)
SDK=${ROOT}/build/SDK

mkdir build
pushd build

echo
echo "======================"
echo "=         1          ="
echo "======================"
mkdir 1
pushd 1
cmake ${ROOT}/1 -DCMAKE_INSTALL_PREFIX=${SDK}/One
cmake --build . --target install
popd

echo
echo "======================"
echo "=         2          ="
echo "======================"
mkdir 2
pushd 2
cmake ${ROOT}/2 -DCMAKE_INSTALL_PREFIX=${SDK}/Two -DCMAKE_PREFIX_PATH=${SDK}
cmake --build . --target install
popd


echo
echo "======================"
echo "=         3          ="
echo "======================"
mkdir 3
pushd 3
cmake ${ROOT}/3 -DCMAKE_INSTALL_PREFIX=${SDK}/Three -DCMAKE_PREFIX_PATH=${SDK}
cmake --build . --target install
popd

popd
