set -e

ROOT=$(pwd)

echo
echo "======================"
echo "=         1          ="
echo "======================"
pushd 1
conan create . cmakedemo/testing
popd

echo
echo "======================"
echo "=         2          ="
echo "======================"
pushd 2
conan create . cmakedemo/testing
popd

echo
echo "======================"
echo "=         3          ="
echo "======================"
pushd 3
conan create . cmakedemo/testing
popd

conan remove -f one/1.0.0@cmakedemo/testing
conan remove -f two/1.0.0@cmakedemo/testing
conan remove -f three/1.0.0@cmakedemo/testing
