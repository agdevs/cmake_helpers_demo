set -e

ROOT=$(pwd)
BUILD=${ROOT}/build
SDK=${BUILD}/SDK

mkdir build
pushd build

echo
echo "======================"
echo "=         1          ="
echo "======================"
mkdir 1
pushd 1
cmake ${ROOT}/1
cmake --build .
popd

echo
echo "======================"
echo "=         2          ="
echo "======================"
mkdir 2
pushd 2
cmake ${ROOT}/2 -DCMAKE_PREFIX_PATH=${BUILD}/1
cmake --build .
popd


echo
echo "======================"
echo "=         3          ="
echo "======================"
mkdir 3
pushd 3
cmake ${ROOT}/3 -DCMAKE_PREFIX_PATH="${BUILD}/2;${BUILD}/1"
cmake --build .
popd

popd
