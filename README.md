# CMake helpers demo

Repository containing several dependent CMake projects each relying on **CMake helpers**.
Those demo projects dependencies are satisfied directly from build tree, from install tree, and
from conan, to validate the correct dependency propagation mechanism in **CMake helpers**.

## Usage

Clone the project with its submodule (cmake_helpers):

    git clone --recurse-submodules ${repo_url} cmake_helpers_demo
    cd cmake_helpers_demo

Run all demos in sequence:

    ./run_demos.sh

