from conans import ConanFile, CMake, tools


class OneConan(ConanFile):
    name = "one"
    version = "1.0.0"
    description = "One library, without dependency"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
    }
    default_options = {
        "shared": False
    }
    exports_sources = "*", "!conanfile.py", "../cmake*"
    build_requires = "cmake_installer/3.14.6@conan/stable"
    build_policy = "missing"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        # Done by the install step in build()
        # see: https://docs.conan.io/en/latest/howtos/cmake_install.html
        pass

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
