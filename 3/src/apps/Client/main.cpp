#include <Compiled/accumulate.h>
#include <HeaderOnly/multiply.h>

#include <iostream>

static constexpr int gMagicNumber = 33;

int main()
{
    std::cout << "Accumulation of " << gMagicNumber << ": "
              << two::accumulate(gMagicNumber)
              << std::endl;

    std::cout << "Decumulation of " << gMagicNumber << ": "
              << two::decumulate(gMagicNumber)
              << std::endl;

    std::cout << "Square of " << gMagicNumber << ": "
              << two::multiply(gMagicNumber, gMagicNumber)
              << std::endl;
    return 0;
}
