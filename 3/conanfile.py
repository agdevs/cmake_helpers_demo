from conans import ConanFile, CMake


class ThreeConan(ConanFile):
    name = "three"
    version = "1.0.0"
    description = "Three client, with external dependency (recurring to other dependencies)"
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
    }
    default_options = {
        "shared": False
    }
    exports_sources = "*", "!conanfile.py", "../cmake*"
    requires = "two/1.0.0@cmakedemo/testing"
    generators = "cmake_paths"
    build_requires = "cmake_installer/3.14.6@conan/stable"
    build_policy = "missing"

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_PROJECT_Three_INCLUDE"] = "conan_paths.cmake"
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        # Done by the install step in build()
        # see: https://docs.conan.io/en/latest/howtos/cmake_install.html
        pass
