#include "sub.h"

namespace two {

int sub(int a, int b)
{
    return a-b;
}

} // namespace two
