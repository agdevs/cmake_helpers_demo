#pragma once

namespace two {

template <class T>
T multiply(T a, T b)
{
    return a*b;
}

} // namespace two
