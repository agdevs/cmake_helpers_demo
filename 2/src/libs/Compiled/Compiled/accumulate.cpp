#include "accumulate.h"

// Same root project, different target
#include <UpstreamCompiled/sub.h>

// Different root project
#include <add.h>

#include <cassert>

namespace two {

int accumulate(int from)
{
    assert(from >= 0);

    int acc = 0;
    for (int i = 0; i != from; ++i)
    {
        acc = one::add(acc, i);
    }
    return acc;
}

int decumulate(int from)
{
    assert(from >= 0);

    int acc = 0;
    for (int i = 0; i != from; ++i)
    {
        acc = sub(acc, i);
    }
    return acc;
}

} // namespace two
