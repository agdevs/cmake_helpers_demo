#pragma once

namespace two {

int accumulate(int from);
int decumulate(int from);

} // namespace two
